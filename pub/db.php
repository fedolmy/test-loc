<?php
include('config.php');
session_start();
function update_db($file, $sql) {
    if ($sql) {
        file_put_contents($file, "$sql", FILE_APPEND);
        return true;
    } else {
        return false;
    }
}

$connect = mysqli_connect($host, $user, $password);
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}
$dbCreateFile = 'createDb.txt';
$createDb = "CREATE DATABASE `$dbName` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci";
if (!file_exists($dbCreateFile)) {
    if (mysqli_query($connect, $createDb)) {
        update_db($dbCreateFile, $createDb);
    } else {
        echo "Can not create the DB" . "<br>";
    }
}
$link = mysqli_connect($host, $user, $password, $dbName);
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}
$dbCreateTableUser = 'createTableUsers.txt';
$createTableUser = "CREATE TABLE `$dbName`.`$table`( 
                    `id` INT(11) NOT NULL AUTO_INCREMENT ,
                    `username` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
                    `pswhash` VARCHAR(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL , 
                    `active` INT(11) NOT NULL , PRIMARY KEY (`id`), UNIQUE (`username`)) ENGINE = InnoDB;";
if (!file_exists($dbCreateTableUser)) {
    if (mysqli_query($link, $createTableUser)) {
        update_db($dbCreateTableUser, $createTableUser);
    } else {
        echo "Can not create the table. \n";
    }
}
?>