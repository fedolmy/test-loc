<?php
$errBD = '';
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$enterDate = $_POST['date'];
	$arrDate = explode('-', $enterDate);
	$year = (int) $arrDate[0];	
	$month = (int) $arrDate[1];
	$day = (int) $arrDate[2];
	$enterBD = mktime(0, 0, 0, $month, $day, $year);
	$minBD=strtotime("5 years ago");
	$maxBD=strtotime("150 years ago");
	if ($minBD < $enterBD){
		$errBD = "Too young";
	}
	if (($maxBD > $enterBD) && ($year >= 101)){
		$errBD = "Too old";
	}
	if ($year < 101){ 
		$errBD = "Too old";
	}
	if(!$errBD){
		require('db.php');
		require('check.php');
		$sqlSel = "SELECT * from `$table` WHERE `username` = '$userName'";
		$result = mysqli_query($link, $sqlSel);
		if (mysqli_num_rows($result) == 0){		
			$sqlIns = "INSERT INTO `$table` (`username`, `pswhash`) VALUES ('$userName', '$psw')";
			$result = mysqli_query($link, $sqlIns);
			mysqli_close($link);
			$_SESSION['userName'] = $userName;
		    header("location: user.php");
		}else{
			$errName = "Such a user already exists". '<br>' ."Enter anoter usernane";
		}
	}
}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>test</title>
		<link rel="stylesheet" href="style.css">
	</head>
	<body>
		<div>
			<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST" style=" text-align: center;
				position: absolute;
				top: 50%;
				left: 50%;
				transform: translate(-50%, -50%);">
				<div class="container" style="padding: 20px 0;">
					<div>
						<?php if (isset($errName)) echo $errName;?>
					</div>
					<label for="uname"><b>Username</b></label>
					<input type="text" placeholder="Enter Username" name="uname" required style=" margin-left: 10px;">
					<br>
					<br>
					<label for="psw"><b>Password</b></label>
					<input type="password" placeholder="Enter Password" name="psw" required>
					<br>
					<br>
					<div>
						<?php if (isset($errBD)) echo $errBD;?>
					</div>
					<label for="date"><b>Date of birth</b></label>					
					<input type="date" name="date" min='101-01-01' required>					
				</div>
				<button type="submit" style="">Register</button>
				<br>
				<br>
				<button type="button" formaction="" style=""><a href="index.php">sign in</a></button>
			</form>
		</div>
	</body>
</html>